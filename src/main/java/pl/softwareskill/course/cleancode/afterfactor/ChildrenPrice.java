package pl.softwareskill.course.cleancode.afterfactor;

public class ChildrenPrice implements Price {
    @Override
    public double getCharge(int daysRented) {
        double result = 1.5;
        if (daysRented > 3) {
            result += (daysRented - 3) * 1.5;
        }
        return result;
    }
}
