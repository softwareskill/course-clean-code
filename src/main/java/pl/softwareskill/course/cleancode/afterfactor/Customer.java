package pl.softwareskill.course.cleancode.afterfactor;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental rental) {
        this.rentals.add(rental);
    }

    public String statement() {
        final StringBuilder result = new StringBuilder("Lista pozycji wypożyczonych przez klienta " + this.name + "\n");

        rentals.forEach(rental -> {
            result.append("\t" + rental.getMovie().getTitle() + "\t");
            result.append(rental.getCharge() + "\n");
        });

        result.append("Należność wynosi: " + getTotalCharge() + "\n");
        result.append("Klient otrzymał " + getTotalRenterPoints() + " punktów stałego klienta");
        return result.toString();
    }

    public int getTotalRenterPoints() {
        return this.rentals.stream()
                .map(Rental::getRenterPoints)
                .reduce(0, Integer::sum);
    }

    public double getTotalCharge() {
        return this.rentals.stream()
                .map(Rental::getCharge)
                .reduce(0.0, Double::sum);
    }
}
