package pl.softwareskill.course.cleancode.afterfactor;

public class Movie {
    private String title;
    private MovieType movieType;
    private Price price;

    public Movie(String title, MovieType movieType) {
        this.title = title;
        this.movieType = movieType;
        this.price = setPrice();
    }

    public String getTitle() {
        return title;
    }

    protected double getCharge(int daysRented) {
        return price.getCharge(daysRented);
    }

    protected int getRenterPoints(int daysRented) {
        return price.getRenterPoints(daysRented);
    }

    private Price setPrice() {
        switch (movieType) {
            case REGULAR:
                return new RegularPrice();
            case NEW_RELEASE:
                return new NewReleasePrice();
            case CHILDREN:
                return new ChildrenPrice();
            default:
                throw new IllegalStateException("Unexpected value: " + movieType);
        }
    }
}
