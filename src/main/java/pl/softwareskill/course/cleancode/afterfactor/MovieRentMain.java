package pl.softwareskill.course.cleancode.afterfactor;

import static pl.softwareskill.course.cleancode.afterfactor.MovieType.CHILDREN;
import static pl.softwareskill.course.cleancode.afterfactor.MovieType.REGULAR;

public class MovieRentMain {

    public static void main(String [] args) {

        Customer customer = new Customer("Andrzej");
        customer.addRental(new Rental(new Movie("Rambo pierwsza krew", REGULAR), 2));
        customer.addRental(new Rental(new Movie("Ucieczka z Alcatraz", REGULAR), 3));
        customer.addRental(new Rental(new Movie("Baśnie", CHILDREN), 10));
        System.out.println(customer.statement());
    }
}
