package pl.softwareskill.course.cleancode.afterfactor;

public enum MovieType {
    CHILDREN, REGULAR, NEW_RELEASE;
}
