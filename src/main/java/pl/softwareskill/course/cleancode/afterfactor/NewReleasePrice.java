package pl.softwareskill.course.cleancode.afterfactor;

public class NewReleasePrice implements Price {
    @Override
    public double getCharge(int daysRented) {
        return daysRented * 3;
    }

    @Override
    public int getRenterPoints(int daysRented) {
        return daysRented > 1 ? 2 : 1;
    }
}
