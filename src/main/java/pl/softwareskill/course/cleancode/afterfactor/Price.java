package pl.softwareskill.course.cleancode.afterfactor;

public interface Price {
    double getCharge(int daysRented);

    default int getRenterPoints(int daysRented) {
        return 1;
    }
}
