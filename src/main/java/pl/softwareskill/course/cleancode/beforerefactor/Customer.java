package pl.softwareskill.course.cleancode.beforerefactor;

import java.util.Enumeration;
import java.util.Vector;

/**
 * Klasa reprezentująca klienta
 */
public class Customer {

    // price codes
    // 2 = dla dzieci
    // 0 = standardowy
    // 1 = nowość

    private String _name; //imię klienta
    private Vector _rentals = new Vector(); //lista wypożyczonych pozycji

    public Customer(String _name) {
        this._name = _name;
    }

    public void add_Rental(Rental _rental) {
        this._rentals.addElement(_rental);
    }

    public String get_name() {
        return _name;
    }

    public String statement() {
        double totalAmount = 0;
        int renterPoints = 0;
        Enumeration rentals = _rentals.elements();
        String result = "Lista pozycji wypożyczonych przez klienta " + get_name() + "\n";

        while(rentals.hasMoreElements()) {
            double thisAmount = 0;
            Rental row = (Rental) rentals.nextElement();

            //określenie opłaty za każdą z pozycji
            switch (row.get_movie().get_price_code()) {
                case 0:
                    thisAmount += 2;
                    if(row.get_days_rented() > 2 ) {
                        thisAmount += (row.get_days_rented() - 2) * 1.5;
                    }
                    break;

                case 1:
                    thisAmount += row.get_days_rented() * 3;
                    break;

                case 2:
                    thisAmount += 1.5;
                    if(row.get_days_rented() > 3) {
                        thisAmount +=  (row.get_days_rented() - 3) * 1.5;
                    }
                    break;
            }

            //dodanie punktów stałego klienta
            renterPoints++;

            //dodanie punktów za wypożyczenie nowości na więcej niż 1 dzień
            if((row.get_movie().get_price_code() == 1) && row.get_days_rented() > 1) {
                renterPoints++;
            }

            //zebranie danych na temat bieżącej pozycji
            result += "\t" + row.get_movie().get_title() + "\t" +
                    String.valueOf(thisAmount) + "\n";

            totalAmount += thisAmount;
        }

        //koniec
        result += "Należność wynosi: " + String.valueOf(totalAmount) + "\n";
        result += "Klient otrzymał " + String.valueOf(renterPoints) + " punktów stałego klienta";
        return result;
    }
}
