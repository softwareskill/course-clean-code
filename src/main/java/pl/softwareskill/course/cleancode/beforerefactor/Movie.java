package pl.softwareskill.course.cleancode.beforerefactor;

/**
 * Klasa reprezentująca film
 */
public class Movie {

    public static final int CHILDRENS = 2; //dla dzieci
    public static final int REGULAR = 0; //standardowy
    public static final int NEW_RELEASE = 1; //nowość

    private String _title; //tytuł
    private int _price_code; //kod ceny

    public Movie(String _title, int _price_code) {
        this._title = _title;
        this._price_code = _price_code;
    }

    public String get_title() {
        return _title;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public int get_price_code() {
        return _price_code;
    }

    public void set_price_code(int _price_code) {
        this._price_code = _price_code;
    }
}
