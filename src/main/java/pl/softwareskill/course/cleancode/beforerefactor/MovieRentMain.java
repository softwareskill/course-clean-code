package pl.softwareskill.course.cleancode.beforerefactor;

public class MovieRentMain {

    public static void main(String [] args) {

        Customer customer = new Customer("Andrzej");
        customer.add_Rental(new Rental(new Movie("Rambo pierwsza krew", 0), 2));
        customer.add_Rental(new Rental(new Movie("Ucieczka z Alcatraz", 0), 3));
        customer.add_Rental(new Rental(new Movie("Baśnie", 2), 10));
        System.out.println(customer.statement());
    }
}
