package pl.softwareskill.course.cleancode.beforerefactor;

/**
 * Klasa reprezentująca wypożyczanie - wypożyczana pozycja
 */
public class Rental {

    private Movie _movie; //film
    private int _days_rented; //liczba dni

    public Rental(Movie _movie, int _days_rented) {
        this._movie = _movie;
        this._days_rented = _days_rented;
    }

    public Movie get_movie() {
        return _movie;
    }

    public void set_movie(Movie _movie) {
        this._movie = _movie;
    }

    public int get_days_rented() {
        return _days_rented;
    }

    public void set_days_rented(int _days_rented) {
        this._days_rented = _days_rented;
    }
}
