package pl.softwareskill.course.cleancode.function;

import static java.util.Collections.emptyMap;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import static java.util.stream.Collectors.toMap;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;

public class HeadersExample {
    protected static Map<String, String> getHttpHeaders(HttpServletRequest request) {
        Map<String, String> httpHeaders = new HashMap<>();

        if (request == null || request.getHeaderNames() == null) {
            return httpHeaders;
        }

        Enumeration names = request.getHeaderNames();
        while (names.hasMoreElements()) {
            String name = (String) names.nextElement();
            String value = request.getHeader(name);
            httpHeaders.put(name.toLowerCase(), value);
        }
        return httpHeaders;
    }
}

class HeadersExampleAfterRefactor {
    protected static Map<String, String> getHttpHeaders(HttpServletRequest request) {
        return isEmptyHeader(request) ? emptyMap() : extractHeaders(request);
    }

    private static boolean isEmptyHeader(HttpServletRequest request) {
        return request == null || request.getHeaderNames() == null;
    }

    private static Map<String, String> extractHeaders(HttpServletRequest request) {
        return Stream.of(request.getHeaderNames())
                .map(String::valueOf)
                .collect(toMap(String::toLowerCase, request::getHeader));
    }
}