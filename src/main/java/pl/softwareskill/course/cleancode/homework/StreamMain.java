package pl.softwareskill.course.cleancode.homework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class StreamMain {

    public static void main(String[] args) {

        //start read file
        String name = "pl.softwareskill.course.cleancode.homework/test.csv";
        List<String[]> list = new ArrayList<>();
        try {
            URL resource = StreamMain.class.getClassLoader().getResource("pl.softwareskill.course.cleancode.homework/test.csv");
            if (resource == null) {
                throw new IllegalArgumentException("file not found!");
            }
            File file = new File(resource.toURI());
            BufferedReader br = new BufferedReader(new FileReader(file));
            try {
                // Read first line
                String line = br.readLine();
                // Make sure file has correct headers
                if (line == null) throw new IllegalArgumentException("File is empty");
                if (!line.equals("email,status,interested"))
                    throw new IllegalArgumentException("File has wrong columns: " + line);
                // Run through following lines
                while ((line = br.readLine()) != null) {
                    // Break line into entries using comma
                    String[] items = line.split(",");
                    try {
                        // If there are too many entries, throw a dummy exception, if
                        // there are too few, the same exception will be thrown later
                        if (items.length > 3) throw new ArrayIndexOutOfBoundsException();
                        // Convert data to person record
                        list.add(items);
                    } catch (ArrayIndexOutOfBoundsException | NumberFormatException | NullPointerException e) {
                        // Caught errors indicate a problem with data format -> Print warning and continue
                        System.out.println("Invalid line: " + line);
                    }
                }
            } finally {
                br.close();
            }
        } catch (Exception e) {
            //somethink was wrong
            e.printStackTrace();
        }

        // after read file
        List<String[]> list2 = new ArrayList<>();
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i)[1].equals("1")) {
                list2.add(list.get(i));
            }
        }

        // print list
        for(int i = 0; i < list2.size(); i++) {
            System.out.print("Person: ");
            for(int j = 0; j < list2.get(0).length; j++) {
                System.out.print(list2.get(i)[j]);
                System.out.print(", ");
            }
            System.out.println("\n");
        }
    }
}
