package pl.softwareskill.course.cleancode2.demeter.bad.domain;

import java.util.UUID;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Builder
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class User {
    UUID userId;
    String name;
    String surname;
    Contact contact;
}
