package pl.softwareskill.course.cleancode2.demeter.good.domain;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class InMemoryUserRepository implements UserRepository {

    private final Set<User> users = new HashSet<>();

    @Override
    public void save(User user) {
        users.add(user);
    }

    @Override
    public Optional<User> findByEmailAddress(String emailAddress) {
        return users.stream().filter(x -> x.getUserEmailAddress().equals(emailAddress)).findAny();
    }
}
