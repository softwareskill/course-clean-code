package pl.softwareskill.course.cleancode2.demeter.good.domain;

import java.util.UUID;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Builder
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class User {
    @Getter
    UUID userId;
    @Getter
    String name;
    @Getter
    String surname;
    Contact contact;

    public String getUserEmailAddress() {
        return contact.getEmailAddress();
    }

    public String getUserTelephone() {
        return contact.getTelephone();
    }
}
