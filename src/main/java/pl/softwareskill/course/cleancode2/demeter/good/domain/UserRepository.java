package pl.softwareskill.course.cleancode2.demeter.good.domain;

import java.util.Optional;

public interface UserRepository {
    void save(User user);
    Optional<User> findByEmailAddress(String emailAddress);
}
