package pl.softwareskill.course.cleancode2.dry.bad.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CreateUserService {

    UserRepository userRepository;

    public void createUser(User user) {
        var emailAddress = user.getUserEmailAddress();
        validateIsUserExists(emailAddress);
        validateEmailAddress(emailAddress);
        userRepository.save(user);
    }

    private void validateIsUserExists(String emailAddress) {
        if (userExists(emailAddress)) {
            throw new IllegalStateException("User with email address " + emailAddress + " already exists");
        }
    }

    private boolean userExists(final String emailAddress) {
        return userRepository.findByEmailAddress(emailAddress).isPresent();
    }

    private void validateEmailAddress(String emailAddress) {
        if (emailAddressNotValid(emailAddress)) {
            throw new IllegalStateException("Email address is not valid");
        }
    }

    private boolean emailAddressNotValid(final String emailAddress) {
        //logic for validate email address
        return true;
    }
}
