package pl.softwareskill.course.cleancode2.dry.bad.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UpdateUserService {

    UserRepository userRepository;

    public void updateUser(User user) {
        var emailAddress = user.getUserEmailAddress();
        validateIsUserExists(emailAddress);
        validateEmailAddress(emailAddress);
        userRepository.update(user);
    }

    private void validateIsUserExists(String emailAddress) {
        if (userNotExists(emailAddress)) {
            throw new IllegalStateException("User with email address " + emailAddress + " not exists");
        }
    }

    private boolean userNotExists(final String emailAddress) {
        return userRepository.findByEmailAddress(emailAddress).isEmpty();
    }

    private void validateEmailAddress(String emailAddress) {
        if (emailAddressNotValid(emailAddress)) {
            throw new IllegalStateException("Email address is not valid");
        }
    }

    private boolean emailAddressNotValid(final String emailAddress) {
        //logic for validate email address
        return true;
    }
}
