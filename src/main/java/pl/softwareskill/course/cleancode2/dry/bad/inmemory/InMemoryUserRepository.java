package pl.softwareskill.course.cleancode2.dry.bad.inmemory;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import pl.softwareskill.course.cleancode2.dry.bad.domain.User;
import pl.softwareskill.course.cleancode2.dry.bad.domain.UserRepository;

public class InMemoryUserRepository implements UserRepository {

    private final Set<User> users = new HashSet<>();

    @Override
    public void save(User user) {
        users.add(user);
    }

    @Override
    public void update(User user) {
        users.add(user);
    }

    @Override
    public Optional<User> findByEmailAddress(String emailAddress) {
        return users.stream().filter(x -> x.getUserEmailAddress().equals(emailAddress)).findAny();
    }
}
