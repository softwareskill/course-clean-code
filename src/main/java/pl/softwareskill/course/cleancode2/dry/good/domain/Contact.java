package pl.softwareskill.course.cleancode2.dry.good.domain;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class Contact {
    String emailAddress;
    String telephone;

    @Builder
    public Contact(String emailAddress, String telephone) {
        this.emailAddress = emailAddress;
        this.telephone = telephone;
        validateEmailAddress();
    }

    private void validateEmailAddress() {
        if (isEmailAddressNotValid()) {
            throw new IllegalStateException("Email address " + this.emailAddress + " is not valid");
        }
    }
    private boolean isEmailAddressNotValid() {
        //logic for validate email address
        return true;
    }
}
