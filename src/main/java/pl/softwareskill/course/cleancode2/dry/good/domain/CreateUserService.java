package pl.softwareskill.course.cleancode2.dry.good.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CreateUserService {

    UserRepository userRepository;

    public void createUser(User user) {
        validateIsUserExists(user);
        userRepository.save(user);
    }

    private void validateIsUserExists(User user) {
        if (userExists(user.getUserEmailAddress())) {
            throw new IllegalStateException("User with email address " + user.getUserEmailAddress() + " already exists");
        }
    }

    private boolean userExists(final String emailAddress) {
        return userRepository.findByEmailAddress(emailAddress).isPresent();
    }
}
