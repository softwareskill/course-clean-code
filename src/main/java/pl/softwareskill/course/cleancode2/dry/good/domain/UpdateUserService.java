package pl.softwareskill.course.cleancode2.dry.good.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UpdateUserService {

    UserRepository userRepository;

    public void updateUser(User user) {
        validateIsUserExists(user);
        userRepository.update(user);
    }

    private void validateIsUserExists(User user) {
        if (userNotExists(user.getUserEmailAddress())) {
            throw new IllegalStateException("User with email address " + user.getUserEmailAddress() + " not exists");
        }
    }

    private boolean userNotExists(final String emailAddress) {
        return userRepository.findByEmailAddress(emailAddress).isEmpty();
    }
}
