package pl.softwareskill.course.cleancode2.dry.good.domain;

import java.util.Optional;

public interface UserRepository {
    void save(User user);
    void update(User user);
    Optional<User> findByEmailAddress(String emailAddress);
}
