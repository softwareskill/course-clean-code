package pl.softwareskill.course.cleancode2.failfast.bad;

import java.math.BigDecimal;

public class PriceCalculator {
    private static final BigDecimal HUNDRED = BigDecimal.valueOf(100);

    public BigDecimal calculateFinalPrice(BigDecimal totalPrice, int discountPercent) {
        final var discount = totalPrice.multiply(BigDecimal.valueOf(discountPercent)).divide(HUNDRED);
        return totalPrice.subtract(discount);
    }
}
