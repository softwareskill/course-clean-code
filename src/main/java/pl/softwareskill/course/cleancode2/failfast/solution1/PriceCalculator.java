package pl.softwareskill.course.cleancode2.failfast.solution1;

import java.math.BigDecimal;

public class PriceCalculator {
    private static final BigDecimal HUNDRED = BigDecimal.valueOf(100);

    public BigDecimal calculateFinalPrice(BigDecimal totalPrice, int discountPercent) {
        validatePercentDiscount(discountPercent);

        var discount = totalPrice.multiply(BigDecimal.valueOf(discountPercent)).divide(HUNDRED);
        return totalPrice.subtract(discount);
    }

    private static void validatePercentDiscount(int discountPercent) {
        if (discountPercent > 100 || discountPercent < 0) {
            throw new IllegalArgumentException("Discount should be between 0 and 100%. Actual value is: " + discountPercent + "%");
        }
    }
}
