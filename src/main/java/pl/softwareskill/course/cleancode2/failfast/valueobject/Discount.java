package pl.softwareskill.course.cleancode2.failfast.valueobject;

import java.math.BigDecimal;

public class Discount {
    private static final BigDecimal HUNDRED = BigDecimal.valueOf(100);
    private final BigDecimal percent;

    Discount(int percent) {
        if (percent > 100 || percent < 0) {
            throw new IllegalArgumentException("Discount should be between 0 and 100%. Actual value is: " + percent + "%");
        }

        this.percent = BigDecimal.valueOf(percent);
    }

    public static Discount ofPercent(int percent) {
        return new Discount(percent);
    }

    public BigDecimal applyTo(BigDecimal value) {
        BigDecimal discount = value.multiply(percent).divide(HUNDRED);
        return value.subtract(discount);
    }
}
