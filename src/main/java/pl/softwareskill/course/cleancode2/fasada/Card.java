package pl.softwareskill.course.cleancode2.fasada;

import static com.google.common.base.Preconditions.checkState;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

class Card implements Serializable {

    private static final CardLimit DEFAULT_CARD_LIMIT = CardLimit.of(BigDecimal.valueOf(500));

    private UUID cardId;
    private String cardNumber;
    private CardLimit limit;
    private boolean isBlocked;
    private boolean isActive;

    private Card(UUID cardId, String cardNumber, CardLimit limit, boolean isBlocked, boolean isActive) {
        this.cardId = cardId;
        this.cardNumber = cardNumber;
        this.limit = limit;
        this.isBlocked = isBlocked;
        this.isActive = isActive;
    }

    public static Card create(UUID cardId, String cardNumber) {
        var startLimit = DEFAULT_CARD_LIMIT;
        var isBlocked = false;
        var isActive = false;
        return new Card(cardId, cardNumber, startLimit, isBlocked, isActive);
    }

    public Card block() {
        checkState(canCardBeBlocked(), "Card can not be blocked");
        this.isBlocked = true;
        return this;
    }

    public Card changeCardLimit(CardLimit newLimit) {
        checkState(canChangeCardLimit(), "Can not change card limit");
        this.limit = newLimit;
        return this;
    }

    public Card activate() {
        checkState(canCardBeActivate(), "Card can not be activate");
        this.isActive = true;
        this.limit = DEFAULT_CARD_LIMIT;
        return this;
    }

    public UUID getCardId() {
        return cardId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public CardLimit getLimit() {
        return limit;
    }

    private boolean canChangeCardLimit() {
        return isActive && !isBlocked;
    }

    private boolean canCardBeBlocked() {
        return isActive && !isBlocked;
    }

    private boolean canCardBeActivate() {
        return !isActive && !isBlocked;
    }

    public CardDto toDto() {
        return CardDto.builder()
                .cardId(this.cardId)
                .cardNumber(this.cardNumber)
                .limit(this.limit)
                .isBlocked(this.isBlocked)
                .isActive(this.isActive)
                .build();
    }
}
