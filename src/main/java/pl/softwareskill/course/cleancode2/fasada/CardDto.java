package pl.softwareskill.course.cleancode2.fasada;

import java.io.Serializable;
import java.util.UUID;
import static lombok.AccessLevel.PUBLIC;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Builder
@FieldDefaults(level = PUBLIC)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardDto implements Serializable {

    UUID cardId;
    String cardNumber;
    CardLimit limit;
    boolean isBlocked;
    boolean isActive;
}
