package pl.softwareskill.course.cleancode2.fasada;

import java.math.BigDecimal;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class CardFacade {

    CardRepository cardRepository;

    public CardDto activate(final UUID cardId) {
        var card = getCard(cardId).activate();
        cardRepository.save(card);
        return card.toDto();
    }

    public CardDto block(final UUID cardId) {
        var card = getCard(cardId).block();
        cardRepository.save(card);
        return card.toDto();
    }

    public CardDto changeCardLimit(final UUID cardId, final BigDecimal valueOfCardLimit) {
        var cardLimit = CardLimit.of(valueOfCardLimit);
        var card = getCard(cardId).changeCardLimit(cardLimit);
        cardRepository.save(card);
        return card.toDto();
    }

    private Card getCard(UUID cardId) {
        return cardRepository.getById(cardId).orElseThrow(() -> new IllegalStateException("Card not exist for cardId: " + cardId));
    }
}
