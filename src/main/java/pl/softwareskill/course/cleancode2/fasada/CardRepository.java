package pl.softwareskill.course.cleancode2.fasada;

import java.util.Optional;
import java.util.UUID;

interface CardRepository {

    Optional<Card> getById(UUID cardId);

    void save(Card card);
}
