package pl.softwareskill.course.cleancode2.kiss.bad;

public class PayUpBad {

    public static void main(String args[]) {
        PayUpBad payUpBad = new PayUpBad();
        Integer result = payUpBad.howMuchToPay("Katowice", 30, 11, "pl_PL", 999);
        System.out.println(result);
    }

    public Integer howMuchToPay(String region, Integer amount, Integer tax, String country, Integer price) {
        if (country == "pl_PL") {
            if (region == "Katowice" || region == "Tychy") {
                if (amount > 10) {
                    price -= ((10 / 100) * price);
                    price += ((tax / 100) * price);
                    return (price * amount);
                }
                return ((price + ((tax / 100) * price)) * amount);
            } else {
                return ((price + ((tax / 100) * price)) * amount);
            }
        } else {
            return (price * amount);
        }
    }
}
