package pl.softwareskill.course.cleancode2.kiss.good;

import java.util.List;

public interface Country {
    String getCode();

    Integer getDiscountAmountPercent();

    Integer getTaxAmountPercent();

    List<String> getDiscountRegions();
}
