package pl.softwareskill.course.cleancode2.kiss.good;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Payment {
    final Integer MINIMUM_AMOUNT_TO_DISCOUNT = 10;
    final Country country;
    final String region;
    final Integer amount;
    Integer nettoPrice;

    public Integer calculate() {
        if (isDiscountRegion(region) && isMinimumAmountToDiscount(amount)) {
            nettoPrice = calculateDiscount(country.getDiscountAmountPercent());
        }
        final Integer bruttoPrice = calculateTax(country.getTaxAmountPercent());
        return bruttoPrice * amount;
    }

    private boolean isDiscountRegion(String region) {
        return country.getDiscountRegions().stream().filter(x -> x.equals(region)).findAny().isPresent();
    }

    private boolean isMinimumAmountToDiscount(Integer amount) {
        return amount > MINIMUM_AMOUNT_TO_DISCOUNT;
    }

    private Integer calculateDiscount(final Integer discount) {
        return (nettoPrice - ((discount / 100) * nettoPrice));
    }

    private Integer calculateTax(Integer tax) {
        return (nettoPrice + (tax / 100 * nettoPrice));
    }
}
