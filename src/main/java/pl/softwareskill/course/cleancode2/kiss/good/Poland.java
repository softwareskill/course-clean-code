package pl.softwareskill.course.cleancode2.kiss.good;

import java.util.List;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import pl.softwareskill.course.cleancode2.kiss.good.Country;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class Poland implements Country {
    String code = "pl_PL";
    Integer discountAmountPercent = 10;
    Integer taxAmountPercent = 23;
    List<String> discountRegions = List.of("Katowice", "Tychy");
}
