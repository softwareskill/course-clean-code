package pl.softwareskill.course.cleancode2.loop;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Builder
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Author {
    String firstName;
    String lastName;
}
