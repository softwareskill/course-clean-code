package pl.softwareskill.course.cleancode2.loop;

import java.util.UUID;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Builder
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Book {
    UUID uuid;
    int publishYear;
    Category category;
    Author author;
}
