package pl.softwareskill.course.cleancode2.loop;

import com.github.javafaker.Faker;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import static java.util.stream.Collectors.toList;
import java.util.stream.IntStream;

public class BookFactory {

    public static List<Book> createRandomBooks(int size) {
        return IntStream.range(0, size)
                .mapToObj(x -> createRandomBook())
                .collect(toList());
    }

    public static Book createRandomBook() {
        return Book.builder()
                .publishYear(generateRandomIntFromRange(2010, 2021))
                .uuid(UUID.randomUUID())
                .author(createRandomAuthor())
                .category(Category.getRandomCategory())
                .build();
    }

    private static int generateRandomIntFromRange(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;

    }

    private static Author createRandomAuthor() {
        Faker faker = new Faker();
        return Author.builder()
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .build();
    }
}
