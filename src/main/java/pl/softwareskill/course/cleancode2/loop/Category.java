package pl.softwareskill.course.cleancode2.loop;

import java.util.Random;

public enum Category {

    SCIENCE_FICTION, FANTASY, ROF_CHILDREN, KITCHEN, HORROR;

    private static final Category[] VALUES = values();
    private static final int SIZE = VALUES.length;
    private static final Random RANDOM = new Random();

    public static Category getRandomCategory() {
        return VALUES[RANDOM.nextInt(SIZE)];
    }
}
