package pl.softwareskill.course.cleancode2.loop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static java.util.stream.Collectors.toList;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StreamApiMain {

    public static void main(String[] args) {
        List<Book> books = BookFactory.createRandomBooks(100);

        List<String> filteredBooksByTraditionalMethod = traditionalFilter(books);
        log.info("Traditional method: " + filteredBooksByTraditionalMethod.toString());

        List<String> filteredBooksByStreamMethod = filterUsingStreams(books);
        log.info("Stream method: " + filteredBooksByStreamMethod.toString());
    }

    private static List<String> traditionalFilter(List<Book> books) {
        List<String> result = new ArrayList<>();

        for (Book book : books) {
            if (book.getPublishYear() != 2020) {
                continue;
            }

            if (book.getCategory() != Category.SCIENCE_FICTION) {
                continue;
            }

            Author author = book.getAuthor();
            String sortableName =
                    String.format("%s, %s",
                            author.getLastName(),
                            author.getFirstName());

            result.add(sortableName);

            if (result.size() == 5) {
                break;
            }
        }

        Collections.sort(result);

        return result;
    }

    private static List<String> filterUsingStreams(List<Book> books) {
        return books.stream()
                .filter(book -> book.getPublishYear() == 2020)
                .filter(book -> book.getCategory().equals(Category.SCIENCE_FICTION))
                .limit(5)
                .map(Book::getAuthor)
                .map(author -> String.format("%s, %s",
                        author.getLastName(),
                        author.getFirstName()))
                .sorted()
                .collect(toList());
    }
}
