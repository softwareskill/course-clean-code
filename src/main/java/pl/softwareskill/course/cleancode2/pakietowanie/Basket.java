package pl.softwareskill.course.cleancode2.pakietowanie;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import static java.util.Objects.requireNonNull;

class Basket {

    private final UUID basketId;
    private final Collection<Product> products;

    public Basket() {
        this.basketId = UUID.randomUUID();
        this.products = new ArrayList<>();
    }

    public UUID getBasketId() {
        return basketId;
    }

    public void insert(Product product) {
        requireNonNull(product);
        products.add(product);
    }

    public Collection<Product> getInsertedProducts() {
        return new ArrayList<>(products);
    }

    public BigDecimal getItemsTotalPrice() {
        return getInsertedProducts()
                .stream()
                .map(Product::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getFinalPrice(DiscountPolicy discountPolicy) {
        BigDecimal totalPrice = getItemsTotalPrice();
        BigDecimal discount = discountPolicy.getDiscount(this);

        return totalPrice.subtract(discount);
    }
}