package pl.softwareskill.course.cleancode2.pakietowanie;

import java.util.Optional;
import java.util.UUID;

interface BasketRepository {

    Optional<Basket> getById(UUID basketId);

    void save(Basket basket);
}
