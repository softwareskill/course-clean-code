package pl.softwareskill.course.cleancode2.pakietowanie;

import lombok.RequiredArgsConstructor;
import java.util.UUID;

@RequiredArgsConstructor
public class CreateOrderService {

    private final BasketRepository basketRepository;
    private final OrderService orderService;
    private final OrderCreationPreconditions orderCreationPreconditions;

    public String createOrder(UUID basketId) {
        Basket basket = basketRepository.getById(basketId)
                .orElseThrow(this::onBasketNotFound);

        checkBasket(basket);

        return orderService.createOrder(basket);
    }

    private RuntimeException onBasketNotFound() {
        return new IllegalArgumentException();
    }

    private void checkBasket(Basket basket) {
        if (orderCreationPreconditions.emptyBasket(basket)) {
            throw new IllegalStateException("Cannot create order from empty basket.");
        }
        if (!orderCreationPreconditions.allProductsAreAvailable(basket)) {
            throw new IllegalStateException("Some product is not available.");
        }
    }
}