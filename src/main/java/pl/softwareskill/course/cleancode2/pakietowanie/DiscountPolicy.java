package pl.softwareskill.course.cleancode2.pakietowanie;

import java.math.BigDecimal;

interface DiscountPolicy {

    BigDecimal getDiscount(Basket basket);
}
