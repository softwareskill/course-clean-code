package pl.softwareskill.course.cleancode2.pakietowanie;


class OrderCreationPreconditions {

    boolean emptyBasket(Basket basket) {
        return basket.getInsertedProducts().isEmpty();
    }

    boolean allProductsAreAvailable(Basket basket) {
        return basket.getInsertedProducts()
                .stream()
                .allMatch(Product::isAvailable);
    }
}
