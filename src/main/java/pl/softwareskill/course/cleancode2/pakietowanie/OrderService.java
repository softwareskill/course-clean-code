package pl.softwareskill.course.cleancode2.pakietowanie;

interface OrderService {

    String createOrder(Basket basket);
}
