package pl.softwareskill.course.cleancode2.pakietowanie;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.util.UUID;

@Value
@Builder
class Product {

    UUID productId;
    String name;
    BigDecimal price;
    boolean available;
}

