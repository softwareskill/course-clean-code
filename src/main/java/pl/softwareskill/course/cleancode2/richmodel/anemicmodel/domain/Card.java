package pl.softwareskill.course.cleancode2.richmodel.anemicmodel.domain;

import java.math.BigDecimal;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Card {
    UUID cardId;
    String cardNumber;
    BigDecimal limit;
    BigDecimal amount;
    boolean isBlocked;
    boolean isActive;
}
