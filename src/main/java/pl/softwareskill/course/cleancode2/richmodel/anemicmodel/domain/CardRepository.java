package pl.softwareskill.course.cleancode2.richmodel.anemicmodel.domain;

import java.util.Optional;
import java.util.UUID;

public interface CardRepository {

    Optional<Card> getById(UUID cardId);
    void save(Card card);
}
