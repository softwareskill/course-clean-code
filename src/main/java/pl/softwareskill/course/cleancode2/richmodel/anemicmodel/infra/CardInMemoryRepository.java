package pl.softwareskill.course.cleancode2.richmodel.anemicmodel.infra;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import pl.softwareskill.course.cleancode2.richmodel.anemicmodel.domain.Card;
import pl.softwareskill.course.cleancode2.richmodel.anemicmodel.domain.CardRepository;

public class CardInMemoryRepository implements CardRepository {

    private final Map<UUID, Card> map = new ConcurrentHashMap<>();

    {
        registerCard(createCard(UUID.fromString("9f12b47b-81c1-4098-9fdb-21d481780a13")));
        registerCard(createCard(UUID.fromString("436d8e1c-2eb8-49f6-a2ae-e85aa57a3f31")));
    }

    private void registerCard(Card card) {
        map.put(card.getCardId(), card);
    }

    @Override
    public Optional<Card> getById(UUID cardId) {
        return Optional.ofNullable(map.get(cardId));
    }

    @Override
    public void save(Card card) {
        registerCard(card);
    }

    private Card createCard(final UUID cardId) {
        Card card = new Card();
        card.setCardId(cardId);
        card.setCardNumber(UUID.randomUUID().toString());
        card.setLimit(BigDecimal.ZERO);
        card.setAmount(BigDecimal.ZERO);
        card.setActive(Boolean.FALSE);
        card.setBlocked(Boolean.FALSE);
        return card;
    }
}
