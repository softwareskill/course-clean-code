package pl.softwareskill.course.cleancode2.richmodel.anemicmodel.service;

import java.math.BigDecimal;
import java.util.UUID;
import pl.softwareskill.course.cleancode2.richmodel.anemicmodel.domain.ActivateCardException;
import pl.softwareskill.course.cleancode2.richmodel.anemicmodel.domain.Card;
import pl.softwareskill.course.cleancode2.richmodel.anemicmodel.domain.CardRepository;

public class CardActivationService {

    private final BigDecimal DEFAULT_CARD_LIMIT = BigDecimal.valueOf(500);
    private final CardRepository cardRepository;

    public CardActivationService(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    public void activateCard(UUID cardId) {
        var card = cardRepository.getById(cardId)
                .orElseThrow(IllegalArgumentException::new);

        validateIsCardBlocked(card);
        validateIsCardActive(card);

        card.setActive(true);
        card.setLimit(DEFAULT_CARD_LIMIT);

        cardRepository.save(card);
    }

    private void validateIsCardBlocked(Card card) {
        if (card.isBlocked()) {
            throw new ActivateCardException();
        }
    }

    private void validateIsCardActive(Card card) {
        if (card.isActive()) {
            throw new ActivateCardException();
        }
    }
}
