package pl.softwareskill.course.cleancode2.richmodel.anemicmodel.service;

import java.util.UUID;
import pl.softwareskill.course.cleancode2.richmodel.anemicmodel.domain.BlockCardException;
import pl.softwareskill.course.cleancode2.richmodel.anemicmodel.domain.Card;
import pl.softwareskill.course.cleancode2.richmodel.anemicmodel.domain.CardRepository;

public class CardBlockerService {

    private final CardRepository cardRepository;

    public CardBlockerService(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    public void block(UUID cardId) {
        var card = cardRepository.getById(cardId)
                .orElseThrow(IllegalArgumentException::new);

        validateIsCardBlocked(card);
        validateIsCardNotActive(card);

        card.setBlocked(true);

        cardRepository.save(card);
    }

    private void validateIsCardNotActive(Card card) {
        if (!card.isActive()) {
            throw new BlockCardException();
        }
    }

    private void validateIsCardBlocked(Card card) {
        if (card.isBlocked()) {
            throw new BlockCardException();
        }
    }
}
