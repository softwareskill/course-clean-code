package pl.softwareskill.course.cleancode2.richmodel.anemicmodel.service;

import java.math.BigDecimal;
import static java.util.Objects.requireNonNull;
import java.util.UUID;
import pl.softwareskill.course.cleancode2.richmodel.anemicmodel.domain.Card;
import pl.softwareskill.course.cleancode2.richmodel.anemicmodel.domain.CardRepository;
import pl.softwareskill.course.cleancode2.richmodel.anemicmodel.domain.ChangeCardLimitException;

public class CardLimiterService {

    private final CardRepository cardRepository;

    public CardLimiterService(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    public void changeCardLimit(UUID cardId, BigDecimal newLimit) {
        var card = cardRepository.getById(cardId)
                .orElseThrow(IllegalArgumentException::new);

        validateIsCardBlocked(card);
        validateIsCardNotActive(card);
        validateCardLimit(newLimit);

        card.setLimit(newLimit);

        cardRepository.save(card);
    }

    private void validateCardLimit(BigDecimal newLimit) {
        requireNonNull(newLimit, "Limit not provided");
        if (newLimit.compareTo(BigDecimal.ONE) < 0) {
            throw new ChangeCardLimitException();
        }
    }

    private void validateIsCardNotActive(Card card) {
        if (!card.isActive()) {
            throw new ChangeCardLimitException();
        }
    }

    private void validateIsCardBlocked(Card card) {
        if (card.isBlocked()) {
            throw new ChangeCardLimitException();
        }
    }
}
