package pl.softwareskill.course.cleancode2.richmodel.richmodel.domain;

import static com.google.common.base.Preconditions.checkState;
import java.math.BigDecimal;
import java.util.UUID;

public class Card {

    private static final CardLimit DEFAULT_CARD_LIMIT = CardLimit.of(BigDecimal.valueOf(500));

    private UUID cardId;
    private String cardNumber;
    private CardLimit limit;
    private boolean isBlocked;
    private boolean isActive;

    private Card(UUID cardId, String cardNumber, CardLimit limit, boolean isBlocked, boolean isActive) {
        this.cardId = cardId;
        this.cardNumber = cardNumber;
        this.limit = limit;
        this.isBlocked = isBlocked;
        this.isActive = isActive;
    }

    public static Card create(UUID cardId, String cardNumber) {
        var startLimit = DEFAULT_CARD_LIMIT;
        var isBlocked = false;
        var isActive = false;
        return new Card(cardId, cardNumber, startLimit, isBlocked, isActive);
    }

    public void block() {
        checkState(canCardBeBlocked(), "Card can not be blocked");
        this.isBlocked = true;
    }

    public void changeCardLimit(CardLimit newLimit) {
        checkState(canChangeCardLimit(), "Can not change card limit");
        this.limit = newLimit;
    }

    public void activate() {
        checkState(canCardBeActivate(), "Card can not be activate");
        this.isActive = true;
        this.limit = DEFAULT_CARD_LIMIT;
    }

    public UUID getCardId() {
        return cardId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public CardLimit getLimit() {
        return limit;
    }

    private boolean canChangeCardLimit() {
        return isActive && !isBlocked;
    }

    private boolean canCardBeBlocked() {
        return isActive && !isBlocked;
    }

    private boolean canCardBeActivate() {
        return !isActive && !isBlocked;
    }
}
