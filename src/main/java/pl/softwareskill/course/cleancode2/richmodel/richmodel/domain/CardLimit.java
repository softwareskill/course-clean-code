package pl.softwareskill.course.cleancode2.richmodel.richmodel.domain;

import static com.google.common.base.Preconditions.checkArgument;
import java.math.BigDecimal;
import static java.math.BigDecimal.ZERO;
import static java.util.Objects.requireNonNull;

public class CardLimit {
    private final BigDecimal value;

    private CardLimit(BigDecimal value) {
        requireNonNull(value, "Limit not provided");
        checkArgument(value.compareTo(ZERO) >= 0, "Limit must be zero or positive.");
        this.value = value;
    }

    public static CardLimit of(final BigDecimal limit) {
        return new CardLimit(limit);
    }

    public BigDecimal getValue() {
        return value;
    }
}
