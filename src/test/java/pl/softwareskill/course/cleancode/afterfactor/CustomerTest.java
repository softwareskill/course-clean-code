package pl.softwareskill.course.cleancode.afterfactor;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static pl.softwareskill.course.cleancode.afterfactor.MovieType.CHILDREN;
import static pl.softwareskill.course.cleancode.afterfactor.MovieType.REGULAR;

@ExtendWith(MockitoExtension.class)
public class CustomerTest {

    @Test
    void calculatesFinalPriceWithPercentDiscount() {
        Customer customer = new Customer("Andrzej");
        customer.addRental(new Rental(new Movie("Rambo pierwsza krew", REGULAR), 2));
        customer.addRental(new Rental(new Movie("Ucieczka z Alcatraz", REGULAR), 3));
        customer.addRental(new Rental(new Movie("Baśnie", CHILDREN), 10));
        var result = customer.statement();

        assertThat(result)
                .isEqualToIgnoringCase("Lista pozycji wypożyczonych przez klienta Andrzej\n" +
                        "\tRambo pierwsza krew\t2.0\n" +
                        "\tUcieczka z Alcatraz\t3.5\n" +
                        "\tBaśnie\t12.0\n" +
                        "Należność wynosi: 17.5\n" +
                        "Klient otrzymał 3 punktów stałego klienta");
    }
}