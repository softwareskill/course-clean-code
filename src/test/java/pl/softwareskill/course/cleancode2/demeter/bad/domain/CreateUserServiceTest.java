package pl.softwareskill.course.cleancode2.demeter.bad.domain;

import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static pl.softwareskill.course.cleancode2.demeter.bad.domain.UserTestBuilder.USER_WITHOUT_CONTACT;
import pl.softwareskill.course.cleancode2.demeter.bad.inmemory.InMemoryUserRepository;

class CreateUserServiceTest {

    UserRepository userRepository = new InMemoryUserRepository();

    @Test
    void createUser() {
        var userWithoutContact = USER_WITHOUT_CONTACT;
        var createUserSerivce = givenCreateUserService(userRepository);

        createUserSerivce.createUser(userWithoutContact);

        verify(userRepository, times(1)).findByEmailAddress(any());
        verify(userRepository, times(1)).save(any());
    }

    private CreateUserService givenCreateUserService(final UserRepository userRepository) {
        return new CreateUserService(userRepository);
    }
}