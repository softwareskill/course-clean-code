package pl.softwareskill.course.cleancode2.failfast.bad;

import java.math.BigDecimal;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.softwareskill.course.cleancode2.failfast.bad.PriceCalculator;

@ExtendWith(MockitoExtension.class)
class PriceCalculatorTest {

    @Test
    void calculatesFinalPriceWithPercentDiscount() {
        var calculator = givenPriceCalculator();
        var totalPrice = new BigDecimal("100.00");
        var discountPercent = 20;

        var finalPrice = calculator.calculateFinalPrice(totalPrice, discountPercent);

        assertThat(finalPrice)
                .isEqualByComparingTo("80");
    }

    @Test
    void calculatesFinalPriceWithPercentDiscountNegativeFinalPrice() {
        var calculator = givenPriceCalculator();
        BigDecimal totalPrice = new BigDecimal("100.00");
        int discountPercent = 200;

        var finalPrice = calculator.calculateFinalPrice(totalPrice, discountPercent);
        //final price is negative!!!!
    }

    private PriceCalculator givenPriceCalculator() {
        return new PriceCalculator();
    }
}