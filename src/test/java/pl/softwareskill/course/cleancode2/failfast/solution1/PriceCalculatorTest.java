package pl.softwareskill.course.cleancode2.failfast.solution1;

import java.math.BigDecimal;
import static org.assertj.core.api.Assertions.assertThatCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PriceCalculatorTest {

    @Test
    void raisesInvalidPercentDiscountAbove100() {
        var calculator = givenPriceCalculator();
        var totalPrice = new BigDecimal("100.00");
        var discountPercent = 200;

        assertThatCode(() -> calculator.calculateFinalPrice(totalPrice, discountPercent))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Discount should be between 0 and 100%. Actual value is: 200%");
    }

    @Test
    void raisesInvalidPercentDiscountBelow0() {
        var calculator = givenPriceCalculator();
        var totalPrice = new BigDecimal("100.00");
        var discountPercent = -50;

        assertThatCode(() -> calculator.calculateFinalPrice(totalPrice, discountPercent))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Discount should be between 0 and 100%. Actual value is: -50%");
    }

    private PriceCalculator givenPriceCalculator() {
        return new PriceCalculator();
    }
}