package pl.softwareskill.course.cleancode2.failfast.valueobject;

import java.math.BigDecimal;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DiscountTest {

    @Test
    void appliesDiscountToPrice() {
        var totalPrice = new BigDecimal("100.00");
        var discount = Discount.ofPercent(20);

        assertThat(discount.applyTo(totalPrice))
                .isEqualByComparingTo("80");
    }

    @Test
    void validatesInvalidPercentDiscountAbove100() {
        assertThatCode(() -> Discount.ofPercent(200))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Discount should be between 0 and 100%. Actual value is: 200%");
    }

    @Test
    void validatesInvalidPercentDiscountBelow0() {
        assertThatCode(() -> Discount.ofPercent(-50))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Discount should be between 0 and 100%. Actual value is: -50%");
    }
}