package pl.softwareskill.course.cleancode2.richmodel.richmodel.domain;

import java.math.BigDecimal;
import java.util.UUID;
import static org.assertj.core.api.Assertions.assertThatCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CardTest {

    private static CardLimit ANY_LIMIT = CardLimit.of(BigDecimal.valueOf(700));

    @Test
    public void canNotBlockAlreadyBlockedCard() {
        var card = givenBlockedCard();
        assertThatCode(() -> card.block())
                .isInstanceOf(IllegalStateException.class);
    }

    @Test
    public void canNotBlockNotActivedCard() {
        var card = givenNotActiveCard();
        assertThatCode(() -> card.block())
                .isInstanceOf(IllegalStateException.class);
    }

    @Test
    public void blocksActivedCard() {
        var card = givenActiveCard();
        assertThatCode(() -> card.block())
                .doesNotThrowAnyException();
    }

    @Test
    public void cannotActivateBlockedCard() {
        var card = givenBlockedCard();
        assertThatCode(() -> card.activate())
                .isInstanceOf(IllegalStateException.class);
    }

    @Test
    public void cannotActivateAlreadyActiveCard() {
        var card = givenActiveCard();
        assertThatCode(() -> card.activate())
                .isInstanceOf(IllegalStateException.class);
    }

    @Test
    public void activatesNewCard() {
        var card = givenNotActiveCard();
        assertThatCode(() -> card.activate())
                .doesNotThrowAnyException();
    }

    @Test
    public void cannotChangeLimitOnBlockedCard() {
        var card = givenBlockedCard();
        assertThatCode(() -> card.changeCardLimit(ANY_LIMIT))
                .isInstanceOf(IllegalStateException.class);
    }

    @Test
    public void changesLimitActiveCard() {
        var card = givenActiveCard();
        assertThatCode(() -> card.changeCardLimit(ANY_LIMIT))
                .doesNotThrowAnyException();
    }

    private Card givenNotActiveCard() {
        return givenCard();
    }

    private Card givenBlockedCard() {
        var card = givenCard();
        card.activate();
        card.block();
        return card;
    }

    private Card givenActiveCard() {
        var card = givenCard();
        card.activate();
        return card;
    }

    private Card givenCard() {
        final var cardId = UUID.randomUUID();
        final var cardNumber = UUID.randomUUID().toString();
        return Card.create(cardId, cardNumber);
    }
}